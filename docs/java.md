# Java

## IDE

Zum Einstellen der Java-11_features in IntelliJ

* Settings -> "Build, Execution, Deployment">Compiler>Java Compiler
* File > Project Structure
* Project Settings > Project

## Downloads

https://jdk.java.net/archive/

## Features

### Java 5 (2004)

* Generics
* Annotations
* Autoboxing

```java
List<Integer> li = new ArrayList<>();
for (int i = 1; i < 50; i += 2)
    li.add(i);
```

* Enumerations
* Varargs

```java
public static void varargs(int... x)
{
   for(int i=0; i < x.length ; i++)
      System.out.println(x[i]);
}
```

* for-each loop

```java
int[] numArray = {10, 20, 30, 40};
 
for(int num : numArray) 
{
        System.out.println(num);
}
```

* Static Imports

```java
import static java.lang.System.out;
```

* `java.util.concurrent`

### Java 6 (2006)

* scripting languages
* jdbc 4.0
* JAXB 2.0

### Java 7 (2011)

* JVm Support für dynamische Sprachen
* switch case für strings

```java
switch (token) 
{
    case ("one"):
        value = "Token one identified";
        break;
    case ("twor"):
        value = "Token twp identified";
        break;
    default:
        throw new IllegalArgumentException();
}
```

* try-catch with Resources
    * dazu neues Interface `AutoCloseable`

```java
try(CustomResource cr = new CustomResource())
    {
        cr.accessResource();
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
```

* Diamond-Operator

```java
Map<String, Integer> params = new HashMap<>();
```

* binary integer literals `0b101010`
* underscore in numeric literals: `123_456`
* einfacheres  Exception-handling mit `|` Pipe (Multi-Catch)
* non-blocking io mit dem nio-Package
    * Buffer
    * Channels
    * Files
    * ...

* WatchService detektiert für regisztrierte Objekte Änderungen und Events
    * Beispiel: Auto-Reload von Properties

```java
final WatchService watchService = FileSystems.getDefault()
            .newWatchService();
    Path path = Paths.get(dirPath);
    path.register(watchService, ENTRY_MODIFY);

    // ...

    watchService.take(); // blocking
    watchService.poll(); // non-blocking (return null if not change was made)
```

### Java 8 (2014)

* lambda-expressions
* Stream-API
    * Stream.of(val1, val2, val3….)
    * Stream.of(arrayOfElements)
    * List.stream()
    * Stream.collect( Collectors.toList() )
    * Stream.toArray( EntryType[]::new )
    * Stream.filter()
    * Stream.map()
    * Stream.sorted()
    * Stream.forEach()
    * Stream.collect()
    * Stream.match() (anyMatch, allMatch, noneMatch)
    * Stream.count()
    * Stream.reduce()
    * Stream.findFirst()
    * parallelStream()
* Funktionale Interfaces (Single Abstract Method interfaces (SAM Interfaces))

```java
@FunctionalInterface
public interface MyFirstFunctionalInterface 
{
    public void firstWork();
}
```

* default methods in Interfaces + statische default methods

```java
public interface Moveable {
    default void move(){
        System.out.println("I am moving");
    }
}
```

* Optionals zur besseren Beschreibung von "Nicht-Vorhandensein"

* Exact Arithmetic Operations
    * Math.(add|substract|multiply|increment|decrement|negate)Exact
    * Math.toIntExact
    * Math.nextDown
* Neue Date-API
    * LocalDate, LocalTime, LocalDateTime
    * Instant, Duration, Period
    * TemporalAdjusters
    * DateTimeFormatterBuilder, DateTimeFormatter

### Java 9 (2017)

* Neues Platform Modul-System (Project Jigsaw)
    * Modularisierung von Java-Code zum besseren Auflösen von Runtime-Dependencies
    * Module als Ersatz von klassischen packages
    * Macht das nativ, wozu man bisher Tools wie maven oder Gradle benötigt hat
* private Methoden in interfaces (als Erweiterung der default-Methoden)
* HTTP/2 Client (als incubator-module)

```java
HttpRequest request = HttpRequest.newBuilder()
  .uri(new URI("https://postman-echo.com/get"))
  .GET()
  .build();
```

* Stream-API Updates
    * takewhile
    * dropWhile
    * Überladung der iterate-Methode um Abbruchkriterium hinzuzufügen

```java
Stream.iterate(1, i -> i <= 10 ,i -> i+1)
```

    * Stream.ofNullable() zum Erzeugen eines leeren Streams wenn null übergeben wird

### Java 10 (2018)

* kaum neue Features
* kleine Verbesserungen + Stabilitäts-Updates
* locale variable type (var) zur weiteren Reduzierung der Geschwätzigkeit von Java

### Java 11 (2018)

* verbessserungen in der HTTP Client API
* Single-File-programme ohne Komilierung ausführen (ohne externe Dependencies)
    *  `java helloWorld.java`
* String-API Änderungen
    * String.repeat(int)
    * .isBlank()
    * .strip(Leading|Trailing)
    * .lines()
* Files.readString() und Files.writeString() zum einfacheren Lesen und Schreiben von Text-Dateien
* Optional.isEmpty()

### Java 12 (2019)

* Collectors.teeing() in Stream API
    * sende stream an zwei Kollektoren
    * dann merge das result mit einer dritten Funktion (BiFunction)
* String-Api Änderungen
    * String.indent()
    * .transform()
 
```java
    String transformedName = name.transform(String::strip)
                            .transform(StringUtils::toCamelCase);
```

* `Files.mismatch(Path, Path)` gibt -1 zurück, wenn die Dateien gleich sind
* `NumberFormat.getCompactNumberInstance(Locale, NumberFormat.Style)` zur kompakten Repräsentierung von großen Zahlen
* Unicode 11 Support (Emoticons und Co.)
